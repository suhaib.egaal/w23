import React from 'react';
import Reporter from './Reporter';

const App = () => {
  return (
    <div>
      
      <Reporter name="Antero Mertaranta">Löikö mörkö sisään</Reporter>

      <Reporter>Löikö mörkö sisään</Reporter>
 
      <Reporter name="Kevin McGran">I know it's a rough time now, but did you at least enjoy playing in the tournament</Reporter>

      <Reporter name="Antero Mertaranta" image="https://images.almatalent.fi/cx0,cy1,cw1039,ch779,570x/https://assets.almatalent.fi/image/8e79d789-1a60-53ae-a0f8-66045fbdc1df">
        Löikö mörkö sisään
      </Reporter>
    </div>
  );
};

export default App;
